# DNSMASQ with HTTPd support #

The main goal of this project is to have http server inside dnsmasq project.

### What is this repository for? ###

* The main purpose is to be able offer files over http protocol for eg in PXE functionality.

### How do I get set up? ###

* Clone this repository by: `git clone https://hetii@bitbucket.org/hetii/dnsmasq.git`
* Goto inside cloned project and run `make`, optionally if you want to disable some feature disable it like `COPTS=-DNO_HTTPD`

### RESULT :) ###

```
LANG=C make && sudo src/dnsmasq --port 0 --no-daemon --enable-tftp --httpd-root-dir=/tmp/ --httpd-listen-port=80
From git://thekelleys.org.uk/dnsmasq
 * branch            HEAD       -> FETCH_HEAD
Already up-to-date.
...
dnsmasq: started, version 2.76-3-gf9e53da DNS disabled
dnsmasq: compile time options: IPv6 GNU-getopt no-DBus no-i18n no-IDN DHCP DHCPv6 no-Lua TFTP no-conntrack ipset auth no-DNSSEC loop-detect inotify HTTPD
dnsmasq-httpd: HTTPD enabled on port 80 with root point to: /tmp/
dnsmasq-tftp: TFTP enabled 
```