#ifndef _HTTPD_H_
#define _HTTPD_H_

void httpd_init(void);
void check_httpd_listeners(void);

#endif