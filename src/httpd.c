/* J. David's webserver */
/* This is a simple webserver.
 * Created November 1999 by J. David Blackstone.
 * CSE 4344 (Network concepts), Prof. Zeigler
 * University of Texas at Arlington
 * Port to dnsmasq by Grzegorz Hetman - ghetman@gmail.com.
 */
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <ctype.h>
#include <strings.h>
#include <string.h>
#include <sys/stat.h>
#include <pthread.h>
#include <sys/wait.h>
#include <stdlib.h>
#include "dnsmasq.h"

#ifdef HAVE_HTTPD

#define ISspace(x) isspace((int)(x))

#define SERVER_STRING "Server: dnsmasq/"VERSION"\r\n"

void accept_request(int, char*);
void bad_request(int);
void cannot_execute(int);
void error_die(const char *);
void execute_cgi(int, const char *, const char *, const char *);
int  get_line(int, char *, int);
void headers(int client, const char *filename, unsigned int length);
void not_found(int client, char *method, const char *path);
void serve_file(int client, char *method, const char *filename);
void unimplemented(int);
void get_mime_type(char* filename, char *out);

/**********************************************************************/
/* A request has caused a call to accept() on the server port to
 * return.  Process the request appropriately.
 * Parameters: the socket connected to the client, root directory path.
 */
/**********************************************************************/
void accept_request(int client, char *root_dir)
{
 char buf[1024];
 int numchars;
 char method[255];
 char url[255];
 char path[512];
 size_t i, j;
 struct stat st;
 int cgi = 0;      /* becomes true if server decides this is a CGI
                    * program */
 char *query_string = NULL;

 numchars = get_line(client, buf, sizeof(buf));
 i = 0; j = 0;
 while (!ISspace(buf[j]) && (i < sizeof(method) - 1))
 {
  method[i] = buf[j];
  i++; j++;
 }
 method[i] = '\0';

 if (strcasecmp(method, "GET") && strcasecmp(method, "POST"))
 {
  unimplemented(client);
  return;
 }

 if (strcasecmp(method, "POST") == 0)
  cgi = 1;

 i = 0;
 while (ISspace(buf[j]) && (j < sizeof(buf)))
  j++;
 while (!ISspace(buf[j]) && (i < sizeof(url) - 1) && (j < sizeof(buf)))
 {
  url[i] = buf[j];
  i++; j++;
 }
 url[i] = '\0';

 if (strcasecmp(method, "GET") == 0)
 {
  query_string = url;
  while ((*query_string != '?') && (*query_string != '\0'))
   query_string++;
  if (*query_string == '?')
  {
   cgi = 1;
   *query_string = '\0';
   query_string++;
  }
}

 sprintf(path, "%s/%s", root_dir, url);
 if (path[strlen(path) - 1] == '/')
  strcat(path, "index.html");
 if (stat(path, &st) == -1) {
  while ((numchars > 0) && strcmp("\n", buf))  /* read & discard headers */
   numchars = get_line(client, buf, sizeof(buf));
  not_found(client, method, path);
 }
 else
 {
  if ((st.st_mode & S_IFMT) == S_IFDIR)
   strcat(path, "/index.html");
  if ((st.st_mode & S_IXUSR) ||
      (st.st_mode & S_IXGRP) ||
      (st.st_mode & S_IXOTH))
  cgi = 1;
  if (strlen(query_string)){
    my_syslog(MS_HTTPD | LOG_INFO, "%s (200): %s (%s)", method, path, query_string);
  } else {
    my_syslog(MS_HTTPD | LOG_INFO, "%s (200): %s", method, path);
  }
  if (!cgi)
   serve_file(client, method, path);
  else
   execute_cgi(client, path, method, query_string);
 }
 close(client);
}

/**********************************************************************/
/* Inform the client that a request it has made has a problem.
 * Parameters: client socket */
/**********************************************************************/
void bad_request(int client)
{
 char buf[1024];

 sprintf(buf, "HTTP/1.0 400 BAD REQUEST\r\n");
 send(client, buf, sizeof(buf), 0);
 sprintf(buf, "Content-type: text/html\r\n");
 send(client, buf, sizeof(buf), 0);
 sprintf(buf, "\r\n");
 send(client, buf, sizeof(buf), 0);
 sprintf(buf, "<P>Your browser sent a bad request, ");
 send(client, buf, sizeof(buf), 0);
 sprintf(buf, "such as a POST without a Content-Length.\r\n");
 send(client, buf, sizeof(buf), 0);
}

/**********************************************************************/
/* Inform the client that a CGI script could not be executed.
 * Parameter: the client socket descriptor. */
/**********************************************************************/
void cannot_execute(int client)
{
 char buf[1024];

 sprintf(buf, "HTTP/1.0 500 Internal Server Error\r\n");
 send(client, buf, strlen(buf), 0);
 sprintf(buf, "Content-type: text/html\r\n");
 send(client, buf, strlen(buf), 0);
 sprintf(buf, "\r\n");
 send(client, buf, strlen(buf), 0);
 sprintf(buf, "<P>Error prohibited CGI execution.\r\n");
 send(client, buf, strlen(buf), 0);
}

/**********************************************************************/
/* Print out an error message with perror() (for system errors; based
 * on value of errno, which indicates system call errors) and exit the
 * program indicating an error. */
/**********************************************************************/
void error_die(const char *sc)
{
 perror(sc);
 exit(1);
}

/**********************************************************************/
/* Execute a CGI script.  Will need to set environment variables as
 * appropriate.
 * Parameters: client socket descriptor
 *             path to the CGI script */
/**********************************************************************/
void execute_cgi(int client, const char *path,
                 const char *method, const char *query_string)
{
 char buf[1024];
 int cgi_output[2];
 int cgi_input[2];
 pid_t pid;
 int status;
 int i;
 char c;
 int numchars = 1;
 int content_length = -1;

 buf[0] = 'A'; buf[1] = '\0';
 if (strcasecmp(method, "GET") == 0)
  while ((numchars > 0) && strcmp("\n", buf))  /* read & discard headers */
   numchars = get_line(client, buf, sizeof(buf));
 else    /* POST */
 {
  numchars = get_line(client, buf, sizeof(buf));
  while ((numchars > 0) && strcmp("\n", buf))
  {
   buf[15] = '\0';
   if (strcasecmp(buf, "Content-Length:") == 0)
    content_length = atoi(&(buf[16]));
   numchars = get_line(client, buf, sizeof(buf));
  }
  if (content_length == -1) {
   my_syslog(MS_HTTPD | LOG_INFO, "%s (400): %s", method, path);
   bad_request(client);
   return;
  }
 }

 sprintf(buf, "HTTP/1.0 200 OK\r\n");
 send(client, buf, strlen(buf), 0);

 if (pipe(cgi_output) < 0) {
  cannot_execute(client);
  return;
 }
 if (pipe(cgi_input) < 0) {
  cannot_execute(client);
  return;
 }

 if ( (pid = fork()) < 0 ) {
  cannot_execute(client);
  return;
 }
 if (pid == 0)  /* child: CGI script */
 {
  char meth_env[255];
  char query_env[255];
  char length_env[255];

  dup2(cgi_output[1], 1);
  dup2(cgi_input[0], 0);
  close(cgi_output[0]);
  close(cgi_input[1]);
  sprintf(meth_env, "REQUEST_METHOD=%s", method);
  putenv(meth_env);
  if (strcasecmp(method, "GET") == 0) {
   sprintf(query_env, "QUERY_STRING=%s", query_string);
   putenv(query_env);
  }
  else {   /* POST */
   sprintf(length_env, "CONTENT_LENGTH=%d", content_length);
   putenv(length_env);
  }
  execl(path, path, NULL);
  exit(0);
 } else {    /* parent */
  close(cgi_output[1]);
  close(cgi_input[0]);
  if (strcasecmp(method, "POST") == 0)
   for (i = 0; i < content_length; i++) {
    recv(client, &c, 1, 0);
    write(cgi_input[1], &c, 1);
   }
  while (read(cgi_output[0], &c, 1) > 0)
   send(client, &c, 1, 0);

  close(cgi_output[0]);
  close(cgi_input[1]);
  waitpid(pid, &status, 0);
 }
}

/**********************************************************************/
/* Get a line from a socket, whether the line ends in a newline,
 * carriage return, or a CRLF combination.  Terminates the string read
 * with a null character.  If no newline indicator is found before the
 * end of the buffer, the string is terminated with a null.  If any of
 * the above three line terminators is read, the last character of the
 * string will be a linefeed and the string will be terminated with a
 * null character.
 * Parameters: the socket descriptor
 *             the buffer to save the data in
 *             the size of the buffer
 * Returns: the number of bytes stored (excluding null) */
/**********************************************************************/
int get_line(int sock, char *buf, int size)
{
 int i = 0;
 char c = '\0';
 int n;

 while ((i < size - 1) && (c != '\n'))
 {
  n = recv(sock, &c, 1, 0);
  /* DEBUG printf("%02X\n", c); */
  if (n > 0)
  {
   if (c == '\r')
   {
    n = recv(sock, &c, 1, MSG_PEEK);
    /* DEBUG printf("%02X\n", c); */
    if ((n > 0) && (c == '\n'))
     recv(sock, &c, 1, 0);
    else
     c = '\n';
   }
   buf[i] = c;
   i++;
  }
  else
   c = '\n';
 }
 buf[i] = '\0';
 
 return(i);
}

/**********************************************************************/
/* Return the informational HTTP headers about a file. */
/* Parameters: the socket to print the headers onthe name of the file */
/**********************************************************************/
void headers(int client, const char *filename, unsigned int length){
 char buf[1024];

 strcpy(buf, "HTTP/1.0 200 OK\r\n");
 send(client, buf, strlen(buf), 0);

 strcpy(buf, SERVER_STRING);
 send(client, buf, strlen(buf), 0);

 get_mime_type(filename, buf);
 my_syslog(MS_HTTPD | LOG_INFO, "Size of the file is: %d, %s", length, buf);
 send(client, buf, strlen(buf), 0);

 sprintf(buf, "Content-Length: %d\r\n", length);
 send(client, buf, strlen(buf), 0);

 strcpy(buf, "\r\n");
 send(client, buf, strlen(buf), 0);
}

/**********************************************************************/
/* Give a client a 404 not found status message. */
/**********************************************************************/
void not_found(int client, char *method, const char *path)
{
  char buf[1024];

  my_syslog(MS_HTTPD | LOG_WARNING, "%s (404): %s", method, path);
  sprintf(buf, "HTTP/1.0 404 NOT FOUND\r\n");
  send(client, buf, strlen(buf), 0);
  sprintf(buf, SERVER_STRING);
  send(client, buf, strlen(buf), 0);
  sprintf(buf, "Content-Type: text/html\r\n");
  send(client, buf, strlen(buf), 0);
  sprintf(buf, "\r\n");
  send(client, buf, strlen(buf), 0);
  sprintf(buf, "<HTML><TITLE>Not Found</TITLE>\r\n");
  send(client, buf, strlen(buf), 0);
  sprintf(buf, "<BODY><P>DNSMASQ server could not fulfill\r\n");
  send(client, buf, strlen(buf), 0);
  sprintf(buf, "your request because the resource specified\r\n");
  send(client, buf, strlen(buf), 0);
  sprintf(buf, "is unavailable or nonexistent.\r\n");
  send(client, buf, strlen(buf), 0);
  sprintf(buf, "</BODY></HTML>\r\n");
  send(client, buf, strlen(buf), 0);
}

void get_mime_type(char* filename, char *out){
  char *part;
  char *mimetype;
  char line[256];
  char *ext = strrchr(filename, '.');
  
  FILE* file = fopen("/etc/mime.types", "r");
  if (file){
    while (fgets(line, sizeof(line), file)){
      strtok(line, "\n");
      part = strtok(line, " \t");
      while( part != NULL ){
        if(!mimetype){
          mimetype = part;
        } else {
        if (strcmp(part, ext+1)==0){
          snprintf(out, 128, "Content-Type: %s\r\n", mimetype);
          return;
        }
      }
        part = strtok(NULL, " \t");
      }
      mimetype = NULL;
    }
    fclose(file);
  }
  snprintf(out, 128, "Content-Type: application/octet-stream\r\n");
}

void serve_file(int client, char *method, const char *filename){
  int BUFFER_SIZE = 4096;
  FILE *source;
  int n;
  int count = 0;

  char buffer[BUFFER_SIZE];
    
  int numchars = 1;
  buffer[0] = 'A'; buffer[1] = '\0';
  /* read & discard headers */
  while ((numchars > 0) && strcmp("\n", buffer))  
  numchars = get_line(client, buffer, sizeof(buffer));

  source = fopen(filename, "rb");
  int fd = fileno(source);
  struct stat buf;
  fstat(fd, &buf);
  int size = buf.st_size;

  headers(client, filename, size);
  if (source) {
      while (!feof(source)) {
          n = fread(buffer, sizeof(unsigned char), BUFFER_SIZE, source);
          count += n;
          send(client, buffer, n, 0);
      }
  } else {
      not_found(client, method, filename);
  }
  fclose(source);
  if (size != count){
    my_syslog(MS_HTTPD | LOG_WARNING, "Transfer just %d from %d bytes of file %s", count, size, filename);
  }
}

/**********************************************************************/
/* Inform the client that the requested web method has not been
 * implemented.
 * Parameter: the client socket */
/**********************************************************************/
void unimplemented(int client)
{
 char buf[1024];

 sprintf(buf, "HTTP/1.0 501 Method Not Implemented\r\n");
 send(client, buf, strlen(buf), 0);
 sprintf(buf, SERVER_STRING);
 send(client, buf, strlen(buf), 0);
 sprintf(buf, "Content-Type: text/html\r\n");
 send(client, buf, strlen(buf), 0);
 sprintf(buf, "\r\n");
 send(client, buf, strlen(buf), 0);
 sprintf(buf, "<HTML><HEAD><TITLE>Method Not Implemented\r\n");
 send(client, buf, strlen(buf), 0);
 sprintf(buf, "</TITLE></HEAD>\r\n");
 send(client, buf, strlen(buf), 0);
 sprintf(buf, "<BODY><P>HTTP request method not supported.\r\n");
 send(client, buf, strlen(buf), 0);
 sprintf(buf, "</BODY></HTML>\r\n");
 send(client, buf, strlen(buf), 0);
}

void check_httpd_listeners(void){
  int client_sock = -1;

  while ((client_sock = accept(daemon->httpdfd, NULL, NULL)) == -1);

  if (client_sock == -1){
    error_die("accept");
  }
  accept_request(client_sock, daemon->httpd_root_dir);
}

void httpd_init(void){
  daemon->httpdfd = 0;
  struct sockaddr_in name;

  daemon->httpdfd = socket(PF_INET, SOCK_STREAM, 0);
    
  if (daemon->httpdfd == -1){
    error_die("Failed to create socket in httpd_init.");
  }
  int rc = 0;
  int oneopt = 1;
#ifdef SO_REUSEPORT
  if ((rc = setsockopt(daemon->httpdfd, SOL_SOCKET, SO_REUSEPORT, &oneopt, sizeof(oneopt))) == -1 && errno == ENOPROTOOPT){
    rc = 0;
  }
#endif

  if (rc != -1){
    rc = setsockopt(daemon->httpdfd, SOL_SOCKET, SO_REUSEADDR, &oneopt, sizeof(oneopt));
  }

  if (rc == -1){
      die(_("Failed to set SO_REUSE{ADDR|PORT} on httpd socket: %s"), NULL, EC_BADNET);
  }

  memset(&name, 0, sizeof(name));
  name.sin_family = AF_INET;
  name.sin_port = htons(daemon->httpd_server_port);
  name.sin_addr.s_addr = htonl(INADDR_ANY);

  if (bind(daemon->httpdfd, (struct sockaddr *)&name, sizeof(name)) < 0){
    error_die("Failed to bind on httpd socket.");
  }

  if (daemon->httpd_server_port == 0){
    unsigned int namelen = sizeof(name);
    if (getsockname(daemon->httpdfd, (struct sockaddr *)&name, &namelen) == -1){
      error_die("Failed to getsockname on httpd socket.");  
    }
    daemon->httpd_server_port = ntohs(name.sin_port);
  }

  if (listen(daemon->httpdfd, 5) < 0){
    error_die("Failed to listen on httpd socket.");
  }
}

#endif